<?php

require('Animals.php');

$animal = new Animals("Shaun");
echo "Name Animals = " . $animal->name . "<br>";
echo "Legs = " . $animal->legs . "<br>";
echo "Cold Blooded = " . $animal->cold_blooded . "<br><br>";

require('frog.php');

$animal = new frog("Buduk");
echo "Name Animals = " . $animal->name . "<br>";
echo "Legs = " . $animal->legs . "<br>";
echo "Cold Blooded = " . $animal->cold_blooded . "<br>";
echo "Jump = " . $animal->jump . "<br><br>";

require('Ape.php');

$animal = new Ape("Kera Sakti");
echo "Name Animals = " . $animal->name . "<br>";
echo "Legs = " . $animal->legs . "<br>";
echo "Cold Blooded = " . $animal->cold_blooded . "<br>";
echo "Yell = " . $animal->yell . "<br><br>";


?>